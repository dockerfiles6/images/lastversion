FROM python:3.11-slim-bullseye as builder
ENV PYROOT=/venv
ENV PYTHONUSERBASE=$PYROOT
WORKDIR /
COPY pyproject.toml ./
RUN apt-get update &&\
    apt-get install --no-install-recommends -y build-essential libffi-dev libssh-dev python3-dev &&\
    pip install --no-cache-dir poetry==1.2.0 setuptools==65.3.0 pip==22.2.2 --upgrade &&\
    poetry lock &&\
    poetry export -f requirements.txt --output requirements.txt &&\
    python3 -m venv venv &&\
    /venv/bin/pip install -r requirements.txt

FROM gcr.io/distroless/python3-debian11@sha256:4e7f0e61fd2bd45064a10ff3e985486b083e139461e16f004baa684eefffea8b
ENV PYTHONPATH=/venv/lib/python3.10/site-packages
COPY --from=builder /venv /venv
ENTRYPOINT ["python", "/venv/bin/lastversion"]
CMD ["python", "/venv/bin/lastversion"]