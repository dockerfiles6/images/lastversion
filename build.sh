#!/usr/bin/env sh
export PRODUCT=lastversion
export SOURCE=lastversion

pip install lastversion --user

export VERSION=$(lastversion ${SOURCE})

docker build --build-arg VERSION=${VERSION} -t ${REGISTRY}/${PRODUCT}:${VERSION} .
